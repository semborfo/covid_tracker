from bokeh.io import curdoc, output_notebook, show, export_png
from bokeh.layouts import widgetbox, row, column
from bokeh.models import GeoJSONDataSource, LinearColorMapper, ColorBar, DateSlider, HoverTool
from bokeh.models.widgets import RadioButtonGroup, Button
from bokeh.palettes import mpl
from bokeh.palettes import brewer
from bokeh.plotting import figure
from datetime import date, timedelta, datetime
import geopandas as gpd
import json
import pandas as pd

URL_CASES = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'
URL_USA_CASES = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv'
URL_DEATHS = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv'
URL_USA_DEATHS = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_US.csv'
URL_RECOVERIES = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv'


def prepare_frame(url):
    df = pd.read_csv(url)
    df = update_covid_frame(df)
    return df


def prepare_us_frame(url):
    df = pd.read_csv(url)
    df = update_covid_us_frame(df)
    return df


def update_covid_us_frame(df):
    USA = 'United States of America'
    DROPLIST = [
        'UID',
        'iso2',
        'iso3',
        'code3',
        'FIPS',
        'Admin2',
        'Province_State',
        'Country_Region',
        'Lat',
        'Long_',
        'Combined_Key',
        'Population'
    ]
    for col in DROPLIST:
        if col in df:
            del df[col]
    result = df.sum(axis=0).to_frame()
    result['Date'] = result.index
    result = result.rename(columns={0: 'value'})
    result = result.reset_index()
    result['country'] = USA
    if 'index' in result:
        del result['index']
    result = result.reindex(['Date', 'country', 'value'], axis=1)
    return result


def update_covid_frame(df):
    DROPLIST = ['Province/State', 'Lat', 'Long']
    for col in DROPLIST:
        del df[col]
    df = df.groupby(['Country/Region']).sum().transpose().stack(0).reset_index()
    df = df.rename(columns={"level_0": "Date"})
    df = df.rename(columns={0: 'value'})
    df = df.rename(columns={"Country/Region": "country"})
    return df


def prepare_update_frame(world_data, usa_data=None):
    ret_data = prepare_frame(world_data)
    if usa_data:
        ret_data = ret_data.append(prepare_us_frame(usa_data))
    return ret_data


def prepare_date(date):
    return date.strftime('%m/%d/%y').lstrip("0").replace("/0", "/")


def convert_sec_to_date(millisec):
    sec = int(millisec / 1000)
    return datetime.fromtimestamp(sec)


def json_data_by_day(date):
    data_type = get_active_label()
    df = dfs[data_type]
    covid_data_on_day = df[df['Date'] == date]
    merged = gdf.merge(covid_data_on_day, on='country', how='left')
    merged.fillna('No data', inplace=True)
    merged_json = json.loads(merged.to_json())
    json_data = json.dumps(merged_json)
    return json_data


def update_plot_by_day(attr, old, new):
    date = convert_sec_to_date(slider.value)
    date = prepare_date(date)
    new_data = json_data_by_day(date)
    geosource.geojson = new_data
    data_type = get_active_label()
    p.title.text = f'Covid in the world, look at {data_type}, day {date}'


def update_plot_by_data_type(attr, old, new):
    update_plot_by_day(attr, old, new)
    switch_pallete()
    switch_colorbar()
    data_type = get_active_label()
    p.title.text = f'Covid in the world, look at {data_type}, day {date}'


def get_active_label():
    try:
        return radio_button_group.labels[radio_button_group.active]
    except NameError:
        return 'Cases'


def switch_pallete():
    p.patches('xs',
              'ys',
              source=geosource,
              fill_color={
                  'field': 'value',
                  'transform': clr_mpprs[get_active_label()]
              },
              line_color='black',
              line_width=0.25,
              fill_alpha=1)


def switch_colorbar():
    # plts = {
    #     'Cases': pltt_cases,
    #     'Deaths': pltt_deaths,
    #     'Recoveries': pltt_recoveries
    # }
    color_bar.color_mapper.palette = plts[get_active_label()]


def animate_update():
    new_date = slider.value + 86400000
    if new_date < slider.end:
        slider.value = int(new_date)


def animate():
    global callback_id
    if button.label == '► Play':
        button.label = '❚❚ Pause'
        callback_id = curdoc().add_periodic_callback(animate_update, 100)
    else:
        curdoc().remove_periodic_callback(callback_id)
        button.label = '► Play'


gdf = gpd.read_file(f'covidetracker/data/ne_110m_admin_0_countries.shp')[['ADMIN', 'ADM0_A3', 'geometry']]
gdf.columns = ['country', 'country_code', 'geometry']

dfs = {
    'Cases': prepare_update_frame(URL_CASES, URL_USA_CASES),
    'Deaths': prepare_update_frame(URL_DEATHS, URL_USA_DEATHS),
    'Recoveries': prepare_update_frame(URL_RECOVERIES)
}

max_cases = dfs['Cases']['value'].max()
max_deaths = dfs['Deaths']['value'].max()
max_recoveries = dfs['Recoveries']['value'].max()

pltt_cases = brewer['OrRd'][9][::-1]
pltt_deaths = brewer['Purples'][9][::-1]
pltt_recoveries = brewer['Blues'][9][::-1]

# palette map
plts = {
    'Cases': pltt_cases,
    'Deaths': pltt_deaths,
    'Recoveries': pltt_recoveries
}

# color_mappers map
clr_mpprs = {
    'Cases': LinearColorMapper(palette=pltt_cases, low=0, high=max_cases, nan_color='#d9d9d9'),
    'Deaths': LinearColorMapper(palette=pltt_deaths, low=0, high=max_deaths, nan_color='#FFFFFF'),
    'Recoveries': LinearColorMapper(palette=pltt_recoveries, low=0, high=max_recoveries, nan_color='#FFFFFF')
}

# Add hover tool
hover = HoverTool(tooltips=[
    ('Country/region', '@country'),
    ('value', '@value'),
])

# Create color bar.
color_bar = ColorBar(color_mapper=clr_mpprs['Cases'],
                     label_standoff=8,
                     width=500,
                     height=20,
                     border_line_color=None,
                     location=(0, 0),
                     orientation='horizontal')

yesterday = date.today() - timedelta(days=1)
yesterday = prepare_date(yesterday)

# Input GeoJSON source that contains features for plotting.
geosource = GeoJSONDataSource(geojson=json_data_by_day(yesterday))

# Create figure object.
p = figure(title=f'Covid in the world, look at Cases, day {yesterday}',
           plot_height=600,
           plot_width=950,
           toolbar_location=None,
           tools=[hover],
           name='figure')

p.xgrid.grid_line_color = None
p.ygrid.grid_line_color = None

# Add patch renderer to figure.
p.patches('xs',
          'ys',
          source=geosource,
          fill_color={
              'field': 'value',
              'transform': clr_mpprs['Cases']
          },
          line_color='black',
          line_width=0.25,
          fill_alpha=1)
# Specify layout
p.add_layout(color_bar, 'below')

# RadioGroupButton
radio_button_group = RadioButtonGroup(labels=['Cases', 'Deaths', 'Recoveries'], active=0)
radio_button_group.on_change('active', update_plot_by_data_type)

# Make a slider object: slider
slider = DateSlider(title='Date',
                    value=yesterday,
                    start=date(2020, 1, 1),
                    end=yesterday,
                    step=1,
                    format='%m/%d/%y')
slider.on_change('value', update_plot_by_day)

button = Button(label='► Play', width=60)
button.on_click(animate)

# Make a column layout of widgetbox(slider) and plot, and add it to the current document

layout = column(p, column(radio_button_group), row([slider, button]))
curdoc().add_root(layout)

# Display plot
show(layout)
